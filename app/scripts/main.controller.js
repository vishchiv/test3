(function() {
    'use strict';

    angular
        .module('test3')
        .controller('MainController', MainController);

    /** @ngInject */
    MainController.$inject = ['$scope', '$timeout'];

    function MainController($scope, $timeout) {
        var vm = this;

        vm.tableData1 = [];
        vm.tableData2 = [];
        vm.chartInputData = [];

        vm.setChartInputData = setChartInputData;

        //wait while table data is initialised
        $timeout(function() {
            vm.setChartInputData(vm.tableData1);
        })

        function setChartInputData(data) {
            vm.chartInputData = data;
        }


    }

})();
