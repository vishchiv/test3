(function() {
    'use strict';

    angular
        .module('test3')
        .config(routeConfig);

    /** @ngInject */
    routeConfig.$inject = ['$stateProvider', '$urlRouterProvider'];

    function routeConfig($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('main', {
                url: '/',
                templateUrl: 'views/main.view.html',
                controller: 'MainController',
                controllerAs: 'vm'
            })
    }

})();