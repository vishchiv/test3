(function() {
    'use strict';

    angular
        .module('test3', [
            'ui.router',
            'ui.bootstrap',
            'ui.bootstrap.tabs'
        ]);

})();
