(function() {
    'use strict';

    angular
        .module('test3')
        .directive('testChart', testChart);

    /** @ngInject */
    testChart.$inject = ['$window', '$timeout'];

    function testChart($window, $timeout) {
        var directive = {
            restrict: 'E',
            template: '<canvas id="chart" class="chart"></canvas>',
            requires: 'TableController',
            scope: {
                chartData: '=',
            },
            link: linkFunction
        }

        return directive;

        function linkFunction(scope, element, attrs, controllers) {
            element.on('$destroy', function() {
                destroyChart();
            });

            var Chart = $window.Chart;
            var myChart;

            element.addClass('chart-container');

            scope.$watch('chartData', function() {
                destroyChart(myChart);
                myChart = createChart();
            })

            function destroyChart(chart) {
                if (!chart) return;
                chart.destroy();
            }

            function createChart() {
                var ctx = element.find('#chart');
                var scatterChart = new Chart(ctx, {
                    type: 'line',
                    data: {
                        datasets: [{
                            label: 'Scatter Dataset',
                            data: scope.chartData
                        }]
                    },
                    options: {
                        scales: {
                            xAxes: [{
                                type: 'linear',
                                position: 'bottom',
                                ticks: {
                                    beginAtZero: true
                                }
                            }],
                        }
                    }
                });

                return scatterChart;

            }


        }
    }

})();
