(function() {
    'use strict';

    angular
        .module('test3')
        .directive('testTable', testTable);

    /** @ngInject */
    testTable.$inject = ['$window'];

    function testTable($window) {
        var directive = {
            restrict: 'E',
            templateUrl: '/views/table.view.html',
            scope: {
                xStart: '=',
                xStep: '=',
                yStart: '=',
                yStep: '=',
                points: '=',
                tableData: '='
            },
            link: linkFunction
        };

        return directive;

        function linkFunction(scope, element, attrs) {
            element.on('$destroy', function() {});

            scope.tableData = buildAxis(scope.xStart, scope.xStep, scope.yStart, scope.yStep, scope.points);

            function buildAxis(xStart, xStep, yStart, yStep, end) {
                var axis = [];

                var xGenerator = generateAxisValues(xStart, xStep),
                    yGenerator = generateAxisValues(yStart, yStep);

                for (var i = 0; i < end; i++) {
                    axis.push({
                        x: xGenerator(),
                        y: yGenerator()
                    })
                }

                return axis;
            }


            function generateAxisValues(start, step) {
                step = step || 1;

                var value = 0;

                return generator;

                function generator() {
                    value = value + step;
                    return value;
                }
            }
        }
    }

})();
